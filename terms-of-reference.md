# Contents

- [Linux Australia Conference Dev Team Terms of Reference](#orgc6975ca)
    - [Purpose/role of the group](#org9caad36)
    - [Membership](#orgb1eaf8a)
    - [Accountability](#orgb55238e)
    - [Working methods/ways of working/meetings](#orgeb50d2e)


<a id="orgc6975ca"></a>

# Linux Australia Conference Dev Team Terms of Reference

<a id="org9caad36"></a>

## Purpose/role of the group

Since ~2007, LCA teams have used conference management software which has been maintained and developed largely by current and past conference teams. From 2007-2016, this was Zookeepr; since 2017 this has been an LA fork of Symposion.

Zookeepr had a small team of developers who worked on it between conferences. Symposion has had a very small team of people working on it for the LCA2018 and LCA2019 teams. As well as working on Symposion, this team has worked on underlying Kubernetes infrastructure, as well as CI/CD and development infrastructure.

Going forward, it is expected that conference teams will be responsible for sourcing infrastructure to run their production, staging, and dev instances of Symposion and the other tools. The dev team may have preferences for what infrastructure is provided and used, but will be expected to work with the conference team's choices.

The 2018/2019 dev teams have benefited from having Gitlab's CI system to provide code review and testing; and from having a Kubernetes cluster in Google Cloud available for a "review app" for every merge request. If conference teams aren't able to provide similar infrastructure, it would be expected that the dev team would source similar infrastructre. Where possible, this will be source from the LA Admin team.

It is expected that conference teams will continue to develop their own branding; the conference dev team may be called on to help apply this branding to the conference sites.

It is expected that conference teams will have a designated Web Lead who coordinates with the dev team. The Web Lead would be the primary person responsible for setting up and troubleshooting issues in the production infrastructure environment, with support from a small subset of the dev team.

In order to faciliate this seperation of concerns between the dev team and the conference team, the dev team will be tasked with producing test data and tooling for rapidly creating a test instance at a specified stage of the conference lifecycle in order to be able to replicate issues without needing to access the productin environment.


<a id="orgb1eaf8a"></a>

## Membership

Intial membership is expected to consist of:

-   Joel Addison (LCA2018 dev team, LCA2020 conf team)
-   Clinton Roy (LCA2020 dev lead)
-   Tobias Schulmann (LCA2019 dev lead)
-   Sachi King (LCA2018 tech lead)
-   James Iseppi (former Zookeepr maintainer)

Membership will be loose and self-nominated; it's expected that some people may only want to provide advice while others actively develop only on certain areas. Membership of the dev team confers no obligation to any particular work; but conference teams may ask specific people to make specific commitments to the conference dev lead.

The team lead will be responsible for reporting to the LA council and to conference teams about the status of the team and development efforts.

The team lead will be responsible for communicating development priorities to the rest of the team, based on an understanding of the overall state of the project as well as the priorities of the conference teams.


<a id="orgb55238e"></a>

## Accountability

As a subcomittee of Linux Australia, the conference dev team will be required to report yearly to the LA council. Reporting to conference teams will be vary depending on the needs of the team.

The council will review the team each year and may ask for changes in leadership or terms of reference, or may disband the team if it has become inactive.


<a id="orgeb50d2e"></a>

## Working methods/ways of working/meetings

So far, all communication has been via the gitlab project at <https://gitlab.com/laconfdev> and the ##laconfdev IRC channel on Freenode.

A mailing list will be requested from LA.

It may be useful to hold a planning session or hackfest near the start of each calendar year, around the same time as Ghosts, in order to plan work for the upcoming LCA team. Budget for this may be requested from the conference team or Council.

A regular monthly catchup online is suggested to coordinate development efforts through the year.